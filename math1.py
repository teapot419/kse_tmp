import matplotlib.pyplot as plt

left_r = 0
right_r = 5
step = 0.01
iters_pre = 1000
iters_draw = 1000
eps = 1e-5
n = int((right_r - left_r) / step) + 2


def f(r, x):
    return r * x * (1 - x)


r = left_r
bifurc_x = []
bifurc_y = []

while r < right_r:
    x = 0.5
    for i in range(iters_pre):
        x = f(r, x)
    all_y = []
    for i in range(iters_draw):
        x = f(r, x)
        all_y.append(x)
    all_y.sort()
    different_y = [all_y[0]]
    for i in range(iters_draw):
        if abs(all_y[i] - different_y[-1]) > eps:
            different_y.append(all_y[i])
    for y in different_y:
        bifurc_x.append(r)
        bifurc_y.append(y)
    for x1 in range(10):
        bifurc_x.append(r)
        bifurc_y.append(x)
    r += step

plt.scatter(bifurc_x, bifurc_y, s=0.1)
plt.show()
