from math import *
from random import randint
from pymsgbox import alert
import matplotlib.pyplot as plt


def f(z):
    return z ** 3 - 1


def df(z):
    return 3 * z ** 2


roots = [complex(1.0, 0.0), complex(-0.5, sqrt(3.0) / 2), complex(-0.5, -sqrt(3.0) / 2)]


def newtons_method(f, df, z0, epsilon):
    l = [z0]
    while len(l) <= 1 or abs(l[-1] - l[-2]) > epsilon:
        l.append(l[-1] - f(l[-1]) / df(l[-1]))
    return l


def root_color(v, roots, colors):
    l = zip(roots, colors)
    mn = min(l, key=lambda t: abs(v - t[0]))
    return mn[1]


epsilon = 1e-2
maxv = 3.0
trackpoints = [complex(maxv, maxv), complex(-maxv, -maxv), complex(0.1, 0.3), complex(- maxv / 2, maxv / 2),
               complex(-maxv, 0.1)]


def random_color():
    return randint(0, 255), randint(0, 255), randint(0, 255)


def visualize():
    import PIL.Image as Image, PIL.ImageDraw as ImageDraw
    size = 512
    img = Image.new("RGB", (size, size))
    d = ImageDraw.ImageDraw(img)

    def to_complex(p):
        return complex(-maxv + p[0] * 2.0 * maxv / size, - maxv + p[1] * 2.0 * maxv / size)

    def from_complex(p):
        return int((p.real + maxv) / (2 * maxv) * size), int((p.imag + maxv) / (2 * maxv) * size)

    for x in range(size):
        for y in range(size):
            v = to_complex((x, y))
            if abs(v) > epsilon:
                t = newtons_method(f, df, v, epsilon)[-1]
                color = root_color(t, roots, [(255, 0, 0), (0, 255, 0), (0, 0, 255)])
                d.point((x, y), fill=color)
    for r in roots:
        rc = from_complex(r)
        d.ellipse((rc[0] - size / 100, rc[1] - size / 100, rc[0] + size / 100, rc[1] + size / 100), fill=(0, 0, 0))
    for p in trackpoints:
        path = list(map(from_complex, newtons_method(f, df, p, epsilon)))
        d.line(path, width=2, fill=random_color())

    del d
    img.save("pilfractal.png")
    ticks1, ticks2 = [0], [0]
    plt.xticks(ticks1, ticks2)
    plt.yticks(ticks1, ticks2)
    plt.imshow(img)

    def onclick(event):
        res = to_complex((event.xdata, event.ydata))
        res = complex(round(res.real, 2), round(res.imag, 2))
        alert(res, "Position")

    plt.connect('button_press_event', onclick)
    plt.show()


visualize()
