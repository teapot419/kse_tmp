import matplotlib.pyplot as plt

iters_pre = 1000
iters_draw = 1000
eps = 1e-5
r = float(input())


def f(x):
    return r * x * (1 - x)


y = 0.5
plot_x = []
plot_y = []
for i in range(iters_pre):
    plot_x.append(i)
    plot_y.append(y)
    y = f(y)
plt.plot(plot_x, plot_y)
plt.show()
