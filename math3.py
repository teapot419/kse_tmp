import matplotlib.pyplot as plt

iters_pre = 1000
iters_draw = 1000
eps = 1e-5
r = float(input())
step = 1e-2


def f(x):
    return r * x * (1 - x)


x = 0.0
plot_x = []
plot_y = []
plot_y2 = []
while x < 1:
    plot_x.append(x)
    plot_y.append(f(x))
    plot_y2.append(x)
    x += step
plt.plot(plot_x, plot_y, plot_x, plot_y2)

plot_x = []
plot_y = []
x = 0.5
for i in range(iters_draw):
    plot_x.append(x)
    plot_y.append(f(x))
    x = f(x)
    plot_x.append(x)
    plot_y.append(x)
plt.plot(plot_x, plot_y)

plt.show()
